# ligo-proxy-utils
Utilities for obtaining short-lived proxy certificates for LIGO

[![pipeline status](https://git.ligo.org/authpublic/ligo-proxy-utils/badges/master/pipeline.svg)](https://git.ligo.org/authpublic/ligo-proxy-utils/commits/master)
[![coverage](https://git.ligo.org/authpublic/ligo-proxy-utils/badges/master/coverage.svg)](https://git.ligo.org/authpublic/ligo-proxy-utils/commits/master)

This package currently provides the following tools

- ligo-proxy-init
